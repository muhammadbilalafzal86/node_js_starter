// Arithmetic Operators
let addition = 5 + 3; // Addition operator (+)
let subtraction = 10 - 4; // Subtraction operator (-)
let multiplication = 2 * 6; // Multiplication operator (*)
let division = 10 / 2; // Division operator (/)
let modulus = 7 % 3; // Modulus operator (%)


// Assignment Operators
let x = 5; // Assignment operator (=)
x += 3; // Addition assignment operator (+=)
x -= 2; // Subtraction assignment operator (-=)
x *= 4; // Multiplication assignment operator (*=)
x /= 2; // Division assignment operator (/=)
x %= 3; // Modulus assignment operator (%=)

// Comparison Operators
let isEqual = 5 == 5; // Equal to operator (==)
let isNotEqual = 5 != 3; // Not equal to operator (!=)
let isStrictEqual = 5 === "5"; // Strict equal to operator (===)
let isStrictNotEqual = 5 !== "5"; // Strict not equal to operator (!==)
let isGreater = 10 > 5; // Greater than operator (>)
let isLess = 3 < 8; // Less than operator (<)
let isGreaterOrEqual = 7 >= 5; // Greater than or equal to operator (>=)
let isLessOrEqual = 2 <= 4; // Less than or equal to operator (<=)

// Logical Operators
let logicalAnd = (true && false); // Logical AND operator (&&)
let logicalOr = (true || false); // Logical OR operator (||)
let logicalNot = !true; // Logical NOT operator (!)


// Unary Operators
let unaryPlus = +5; // Unary plus operator (+)
let unaryMinus = -3; // Unary minus operator (-)
let logicalNotUnary = !true; // Logical NOT operator (!)
let bitwiseNotUnary = ~7; // Bitwise NOT operator (~)
let typeofOperator = typeof x; // typeof operator
let deleteOperator = delete x; // delete operator

// Ternary Operator
let condition = (x > 10) ? "x is greater" : "x is not greater"; // Ternary operator (?:)

// Bitwise Oprator.
//  Bitwise AND (&): Performs a bitwise AND operation between two numbers, 
// comparing each corresponding bit and producing a new number where each bit is set if both corresponding bits are set.
const result = 5 & 3; // Binary: 0101 & 0011 = 0001
console.log(result); // Output: 1

// Bitwise OR (|):
const result1 = 5 | 3; // Binary: 0101 | 0011 = 0111
console.log(result1); // Output: 7

// Bitwise XOR (^):
const result2 = 5 ^ 3; // Binary: 0101 ^ 0011 = 0110
console.log(result2); // Output: 6

// Bitwise NOT (~): 
const result3 = ~5; // Binary: ~0101 = 1010
console.log(result3); // Output: -6 (in two's complement form)

// Left Shift (<<): 
const result4 = 5 << 2; // Binary: 0101 << 2 = 010100 (20 in decimal)
console.log(result4); // Output: 20

// Right Shift (>>):
const result5 = 5 >> 1; // Binary: 0101 >> 1 = 0010 (2 in decimal)
console.log(result5); // Output: 2

// Unsigned Right Shift (>>>):
const result6 = -5 >>> 1; // Binary: -0101 >>> 1 = 0110 (6 in decimal)
console.log(result6); // Output: 6






