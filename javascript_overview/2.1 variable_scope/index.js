let x;       // Now x is undefined
x = 5;       // Now x is a Number
x = "John";  // Now x is a String
// String Data Type . we can use double as well as single quotes for declaration.
// Using double quotes:
let Name = "Bilal";
// Using single quotes:
let Name2 = 'ALi';
// Number Data Type.
// All JavaScript numbers are stored as decimal numbers (floating point).Numbers can be written with, or without decimals:
let x1 = 34.00;
let x2 = 34;
let x3 = 3.14;

// BigInt.
// JavaScript BigInt is a new datatype (ES2020) that can be used to store integer values that are too big to be represented by a normal JavaScript Number.
let y = BigInt("123456789012345678901234567890");
console.log(y);

// JavaScript Booleans. Booleans can only have two values: true or false.
let x4 = 5;
let y2 = 5;
let z = 6;
(x4 == y2)       // Returns true
(x4 == z)       // Returns false

// JavaScript Objects. JavaScript objects are written with curly braces {}.Object properties are written as name:value pairs, separated by commas.
const person = {firstName:"M", lastName:"Bilal", age:25, eyeColor:"black"};

// SCOPE 

// Global scope variables
var globalVariable = "I'm a global variable"; // Declared using 'var'
let globalLetVariable = "I'm a global let variable"; // Declared using 'let'
const globalConstVariable = "I'm a global const variable"; // Declared using 'const'

function exampleFunction() {
  // Local scope variables
  var localVariable = "I'm a local variable"; // Declared using 'var'
  let localLetVariable = "I'm a local let variable"; // Declared using 'let'
  const localConstVariable = "I'm a local const variable"; // Declared using 'const'

  console.log(localVariable); // Accessible within the function scope
  console.log(localLetVariable); // Accessible within the function scope
  console.log(localConstVariable); // Accessible within the function scope

  console.log(globalVariable); // Accessible within the function scope
  console.log(globalLetVariable); // Accessible within the function scope
  console.log(globalConstVariable); // Accessible within the function scope
}

exampleFunction();

console.log(globalVariable); // Accessible in the global scope
console.log(globalLetVariable); // Accessible in the global scope
console.log(globalConstVariable); // Accessible in the global scope

// Trying to access local variables outside their scope will result in an error
// console.log(localVariable); // Throws an error: ReferenceError: localVariable is not defined
// console.log(localLetVariable); // Throws an error: ReferenceError: localLetVariable is not defined
// console.log(localConstVariable); // Throws an error: ReferenceError: localConstVariable is not defined
