function Person(fName, lName) {

  this.firstName = fName,
    this.lastName = lName,

    this.FullName = function () {
      return this.firstName + ' ' + this.lastName;
    }
};

// Create objects
const person1 = new Person("M", "Bilal");
const person2 = new Person("M", "Faizan");

// Access method
console.log(person1.FullName());
console.log(person2.FullName());