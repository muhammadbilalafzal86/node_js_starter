function Multiplier(number) {
  return function(value) {
    return number * value;
  };
}
const multiplier = Multiplier(5);
console.log(multiplier(10));
