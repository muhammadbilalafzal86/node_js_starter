// Statements
let x = 5; // Variable declaration and assignment (statement)
if (x === 5) { // Conditional statement (statement)
  console.log("x is 5"); // Function call within statement
}

// Expressions
x + 3; // Arithmetic expression (expression)
x === 5; // Comparison expression (expression)
Math.max(10, 20); // Function call expression (expression)
