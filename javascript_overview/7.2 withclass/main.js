class Person {
  constructor(fName, lName) {
    this.firstName = fName;
    this.lastName = lName;
  }

  FullName() {
    return `${this.firstName} ${this.lastName}`;
  }
}
const person = new Person('M', 'Bilal');
console.log(person.FullName());