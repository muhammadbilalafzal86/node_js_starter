
const per = 55;


let grade;

if (per >= 90) {
  grade = 'A';
} else if (per >= 80) {
  grade = 'B';
} else if (per >= 70) {
  grade = 'C';
} else if (per >= 60) {
  grade = 'D';
} else {
  grade = 'F';
}


console.log(`The grade for ${per}% is ${grade}`);