// take an array
const num1 = [22, 55, 4, 5, 55, 5]
const num = [0, 1, 2, 8, 4, 5];
num.at(3);              // first of all we see at() method it will behave like num[3] like. it return the index value.
num.concat(num1);           // 2nd method is concate(). it will join 2 or more arrays and return a single new array.
num.copyWithin(0, 3, 4)  // The copyWithin() method shallow copies part of an array to another location in the same array and returns it without modifying its length.
const num2 = num.entries();
num2.next().value       // The entries() method returns a new array iterator object that contains the key/value pairs for each index in the array.
num.every(isBigEnough);         // The every() method tests whether all elements in the array pass the test implemented by the provided function. It returns a Boolean value.
num.fill(9, 3, 8)       // array.fill() this method will fill the array with given digit . it will contain three pera meter maximum(fill with,from where,till where)
num.filter(nm => nm > 3)        //  array.filter() this method itrate the whole array and return a new array from that array on a specific condition.
num.find(nm => nm > 3)      // find() this method will work like filter but only return a very first value that match our requirements.
num.findIndex(nm => nm > 3)     // findIndex() this method will return the index number of that first value which match given condition.
num.findLast(nm => nm > 3)      // findlast() this method will return the last value that match our condition.
num.findLastIndex(nm => nm > 3)     // findlastIndex() this method will return the last index number that match our condition.
num.flat()      // flat() this method will concate all the sub array elements and return a new array.
num.forEach(myFunction);        // forEach(). This method will perform some specific function on each element.
const myArr = Array.from(text);     // Array.from().The Array.from() method returns an array from any object with a length property.
num.includes(3)     // includes(). The includes() method returns true if an array contains a specified value.
num.indexOf(3);     // indexOf(). it will return the index number of the given value.
Array.isArray(num)      // isArray(). The isArray() method returns true if an object is an array, otherwise false.
num.join()      // join(). The join() method returns an array as a string.
num.length      // length. The length property sets or returns the number of elements in an array.
const num3 = num.keys();
for (const key of num3) {
    console.log(key);     // The keys() method returns a new array iterator object that contains the keys for each index in the array.
}       
num.map(Math.sqrt)      // map(). map() creates a new array from calling a function for every array element.
num.pop()       // pop(). The pop() method removes (pops) the last element of an array.
num.push(9);        //   push(). The push() method adds new items to the end of an array.
Array.of('foo', 2, 'bar', true)     // The Array.of() static method creates a new Array instance from a variable number of arguments, regardless of number or type of the arguments.
num.reduce(myFunc);     // The reduce() method executes a user-supplied "reducer" callback function on each element of the array.
num.reduceRight(myFunc);        // The reduceRight() method applies a function against an accumulator and each value of the array (from right-to-left) to reduce it to a single value.
num.reverse();      // reverse(). The reverse() method reverses the order of the elements in an array.
num.shift();        // shift(). The shift() method removes the first item of an array.
num.slice(2, 4);       // slice(). The slice() method returns selected elements in an array, as a new array.
num.some(even);     // The some() method tests whether at least one element in the array passes the test implemented by the provided function.
num.sort();         // this method will sort our array in an order.
num.splice(1,0,8);      // The splice() method changes the contents of an array by removing or replacing existing elements and/or adding new elements
num.toReversed()        // The toReversed() method of Array instances is the copying counterpart of the reverse() method. It returns a new array with the elements in reversed order.
num.toSorted();     // The toSorted() method of Array instances is the copying version of the sort() method. It returns a new array with the elements sorted in ascending order.
num.toSplice(1,0,8);        // The toSpliced() method of Array instances is the copying version of the splice() method. It returns a new array with some elements removed and/or replaced at a given index.
num.toString();         // The toString() method returns a string representing the specified array and its elements.
num.unshift(0);         // The unshift() method adds the specified elements to the beginning of an array and returns the new length of the array.
const num4 = num.values();
for (const value of num4) {
  console.log(value);        // The values() method returns a new array iterator object that iterates the value of each item in the array.
}                   

array.with(index, value)        // The with() method of Array instances is the copying version of using the bracket notation to change the value of a given index. It returns a new array with the element at the given index replaced with the given value.
